# A calculator for words
Calculates a mathematical statement in words.

The program supports numbers in the range -999,999,999,999,999 to 999.999.999.999.999 (trillion).

Each word should be separated with spaces, e.g.
```
nine = 9
forty nine = 49
hundred forty nine = 149
five hundred forty nine = 549
minus hundred = -100
```

There are four available operators: plus, minus, times, over.

## Build
The project is build using CMake with the provided CMakeLists.txt

## Usage examples
Plus
```
$ ./build/word_calc "five plus two"
5 + 2 = 7
five plus two equals seven
```
Minus
```
$ ./build/word_calc "minus hundred minus seventy five"
-100 - 75 = -175
minus hundred minus seventy five equals minus hundred seventy five
```
Times
```
$ ./build/word_calc "five hundred forty nine times seventy eight"
549 * 78 = 42822
five hundred forty nine times seventy eight equals  hundred twenty two
```
Over (divided by)
```
$ ./build/word_calc "four hundred twenty four over eleven"
424 / 11 = 38.545
four hundred twenty four over eleven equals thirty eight point five four five
```

Big numbers
```
$ ./build/word_calc "five hundred trillion over seventy three"
500000000000000 / 73 = 6849315068493.15
five hundred trillion over seventy three equals six trillion eight hundred forty nine billion three hundred fifteen million sixty eight thousand four hundred ninety three point one five
```

## Contributors
Eivind Vold Aunebakk
