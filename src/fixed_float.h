#pragma once
#include <string>
#include <iostream>
#include <iomanip>

// Converts a floating point number to a string removing trailing zeros
std::string fixed_float(double x)
{
    std::ostringstream ss;
    ss << std::fixed << std::setprecision(std::cout.precision()) << x;
    std::string str = ss.str();

    int last = str.find_last_not_of('0') + 1;
    if (str[last - 1] == '.') last--;
    return str.substr(0, last);
}
