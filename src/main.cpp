#include <iostream>
#include <sstream>
#include <string>
#include <array>
#include <vector>
#include <algorithm>
#include <cmath>
#include "fixed_float.h"


const std::vector<std::string> digits1 = {"zero",    "one",     "two",       "three",    "four",
                                          "five",    "six",     "seven",     "eight",    "nine",
                                          "ten",     "eleven",  "twelve ",   "thirteen", "fourteen",
                                          "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};

const std::vector<std::string> digits10 = {"twenty", "thirty",  "forty",  "fifty",
                                           "sixty",  "seventy", "eighty", "ninety"};

const std::string hundred = "hundred";

const std::vector<std::string> digits1000 = {"thousand", "million", "billion", "trillion"};

const std::string minus = "minus";

// The operators. The index in the string array match the enum value.
const std::vector<std::string> operators        = {"plus", "minus", "times", "over"};
const std::vector<std::string> operator_symbols = {"+", "-", "*", "/"};
enum class Operator
{
    add,
    sub,
    mul,
    div
};

// The maximum number of decimals we print
const size_t max_decimals = 3;

// Tries to parse a single 999 block of a number
// E.g: "four hundred seventy six thousand two hundred four"(476,204) is parsed as 476 and the cursor is moved to "thousand"
double parse_hundred(const std::vector<std::string>& parts, size_t& cursor)
{
    // the number we're summing into
    double num = 0;

    // For each word
    for (; cursor < parts.size(); cursor++)
    {
        const std::string& s = parts[cursor];

        // If the word is 0-19 add it to the sum
        auto it = std::find(digits1.begin(), digits1.end(), s);
        if (it != digits1.end())
        {
            num += it - digits1.begin();
            continue;
        }

        // If we have 20, 30, 40 ect. add it to the sum
        it = std::find(digits10.begin(), digits10.end(), s);
        if (it != digits10.end())
        {
            num += 10 * (2 + it - digits10.begin());
            continue;
        }

        // If we have a hundred we multiply the previous number by 100,
        // unless if it's 0 in which case we simply have a hundred
        if (s == hundred)
        {
            if (num == 0)
                num = 100;
            else
                num *= 100;
            continue;
        }

        // If the word is not part of a number we're done
        break;
    }

    return num;
}

// Tries to parse a number
double parse_number(const std::vector<std::string>& parts, size_t& cursor)
{
    // Remember where we started reading from.
    const size_t start_pos = cursor;

    // Check if the first word is minus
    bool isMinus = false;
    if (parts[cursor] == minus)
    {
        isMinus = true;
        cursor++;
    }

    // The number we're summing into
    double num = 0;

    // While theres more words
    while (cursor < parts.size())
    {
        // Get the first 999 part
        double hundred = parse_hundred(parts, cursor);

        // If theres more words
        if (cursor < parts.size())
        {
            // Check if the next word is thousand, million, ect.
            const std::string& s = parts[cursor];
            auto it              = std::find(digits1000.begin(), digits1000.end(), s);
            if (it != digits1000.end())
            {
                cursor++;
                double p = std::pow(1000, it - digits1000.begin() + 1);
                if (hundred == 0) hundred = 1;
                hundred *= p;
            }
        }
        // If we got a number add it to the sum
        // If we failed to get a number from the current word, it's not a part of the number and we'll break
        if (hundred > 0)
            num += hundred;
        else
            break;
    }


    // Negate the number if the first word was minus
    if (isMinus)
    {
        num *= -1;
    }

    // Make sure we've actually read anything
    if (cursor == start_pos)
    {
        if (cursor != parts.size())
            throw std::runtime_error("Failed to parse number \"" + parts[cursor] + "\"");
        else
            throw std::runtime_error("Failed to parse number");
    }

    return num;
}

// Tries to parse the operator.
Operator parse_operator(const std::vector<std::string>& parts, size_t& cursor)
{
    const auto it = std::find(operators.begin(), operators.end(), parts[cursor]);
    if (it == operators.end())
    {
        if (cursor >= parts.size())
            throw std::runtime_error("Missing operator");
        else
            throw std::runtime_error("Failed to parse operator \"" + parts[cursor] + "\"");
    }
    cursor++;
    return (Operator)(it - operators.begin());
}

// Calculates the result based on the given operator.
double calc(double a, double b, Operator op)
{
    switch (op)
    {
        case Operator::add: return a + b;
        case Operator::sub: return a - b;
        case Operator::mul: return a * b;
        case Operator::div: return a / b;
        default: throw std::runtime_error("Unknown operator");
    }
}

// Converts a number into words
std::string number_to_string(double number)
{
    std::stringstream ss;

    if (number < 0)
    {
        ss << "minus ";
        number *= -1;
    }

    if (number >= 1000)
    {
        // For each 999 part we do a recursive call to convert one part at a time
        for (int i = digits1000.size() - 1; i >= 0; i--)
        {
            double p = std::pow(1000, i + 1);
            if (number < p) continue;

            long h = (long)number / p;
            number -= h * p;

            if (h >= 1)
            {
                ss << number_to_string(h) << " ";
            }
            ss << digits1000[i] << " ";
        }
    }
    if (number >= 100)
    {
        long h = (long)number / 100;
        number -= h * 100;

        if (h >= 1) ss << digits1[h] << " ";
        ss << hundred << " ";
    }
    if (number >= 20)
    {
        long h = (long)number / 10;
        number -= h * 10;

        if (h >= 2)
        {
            ss << digits10[h - 2] << " ";
        }
    }

    if (number >= 1)
    {
        long h = (long)number;
        number -= h;

        ss << digits1[h] << " ";
    }

    // Add decimals
    if (number > 0)
    {
        ss << "point ";
        for (size_t i = 0; i < max_decimals && number > 0; i++)
        {
            number *= 10;
            long h = (long)number;
            if (i == max_decimals - 1)
            {
                // Round the last decimal
                h = std::round(number);
                if (h == 0) break;
            }
            number -= h;
            ss << digits1[h] << " ";
        }
    }
    auto str = ss.str();
    str.pop_back(); // Remove the last space
    return str;
}

int main(int argc, char** argv)
{
    // Configure output such that numbers printed as numbers have the 
    // same amount of decimals as the numbers printed with words.
    std::cout << std::setprecision(max_decimals);

    // Get the input.
    if (argc < 2)
    {
        return 0;
    }
    const std::string input(argv[1]);

    // Split at spaces.
    std::vector<std::string> parts;
    std::istringstream iss(input);
    std::string s;
    while (getline(iss, s, ' '))
    {
        parts.push_back(s);
    }

    // Which part we're at. The cursor is moved forward by the parse functions
    // such that the next parser can continue where the previous one ended.
    size_t cursor = 0;

    // Parse.
    try
    {
        const double num1 = parse_number(parts, cursor);
        const Operator op = parse_operator(parts, cursor);
        const double num2 = parse_number(parts, cursor);

        // Calculate the result.
        const double res = calc(num1, num2, op);

        // Print with numbers and symbols
        std::cout << fixed_float(num1) << " " << operator_symbols[(int)op] << " " << fixed_float(num2) << " = "
                  << fixed_float(res) << std::endl;

        // Print with text
        std::cout << number_to_string(num1) << " " << operators[(int)op] << " " << number_to_string(num2) << " equals "
                  << number_to_string(res) << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}
